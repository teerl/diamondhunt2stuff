setTimeout (function ()  {
    let seedsToCheck = "wheat redMushroom dottedGreenLeaf snapegrass greenLeaf limeLeaf blewitMushroom carrot tomato".split(' ');
    let defaultTime = 500;
    let clickyChain = function(datas, timeDelay, afterFunc) {
        setTimeout(function() {
            if (datas.length > 0) {
                let data = datas.shift();
                if (typeof data == "string") {
                    data = document.querySelector(data);
                }
                data.click();
                clickyChain(datas, timeDelay, afterFunc);
            }
            else if (afterFunc !== undefined) {
                afterFunc ();
            }
        }, timeDelay);
    };
    let searchFieldText = function(text, callback) {
        let ret = [];
        for (num of [1, 2, 3, 4]) {
            let patch = document.querySelector("#farming-patch-area-" + num);
            if (patch.innerText == text) {
                ret.push (patch);
            }
        }
        if (callback !== undefined) {
            callback(ret);
        } else {
            return ret;
        }
    };
    clickyChain ([document.querySelector("#tab-container-bar-farming")], defaultTime, function () {
        let openFields = searchFieldText("Click to grow");
        let harvestFields = searchFieldText("Click to harvest");
        let removeFields = searchFieldText("Click to remove");
        // It there is something to do with thefields
        if (openFields.length > 0 || harvestFields.length > 0 || removeFields.length > 0) {
            console.log("PLANTING");
            let plantThisSeed = function() {
                for (seed of seedsToCheck) {
                    console.log("Checking seed", seed);
                    let seedCount = document.querySelector("#item-box-" + seed + "Seeds > span").innerText;
                    if (0 < seedCount) {
                        return seed;
                    }
                }
            }(); // A function call
            if (plantThisSeed !== undefined) { // IF there's a seed to plant
                let planter = document.getElementById('item-box-planter')
                if (planter.display != 'none') {
                    //
                    // Planting with the planter
                    //
                    clickyChain([planter, '#dialogue-plant-' + plantThisSeed + 'Seeds'], defaultTime, function() {
                        let clickUs = ['#dialogue-loot > input[type=button]'];
                        let dialog = document.querySelector("#dialogue-confirm");
                        if (dialog.parentElement.style.display != 'none') { // check for harvest dialog
                            let btn = dialog.querySelector("#dialogue-confirm-text > input[value='Harvest & Replant']")
                            if (btn !== undefined && btn !== null) {
                                console.log("Harvest & Replant!");
                                clickUs.unshift(btn);
                            }
                        }
                        clickyChain(clickUs, defaultTime);
                    });
                } else {
                    // Planting without the planter
                    console.log('Planting without planter not supported yet');
                }
            }
        } else {
            console.log("No available fields");
        }
        // Check if the timeout interval shoud be updated
        chrome.storage.local.get(['autoPlantingInterval'], function(response) {
            console.log(response);
            if (response['autoPlantingInterval'] === undefined) {return;}
            let calcInterval = function () {
                if (openFields.length > 0 || harvestFields.length > 0 || removeFields.length > 0) {return 20000;}
                let newTime = undefined;
                for (num of [1, 2, 3, 4]) {
                    let patchTime = document.querySelector("#farming-patch-area-" + num).innerText.match(/Growing \((.*)\)/);
                    if (patchTime === undefined || patchTime === null) {continue}
                    let timeMatch = patchTime[1].match("(([0-9]+):)?([0-9]+):([0-9]+)");
                    if (timeMatch === undefined || timeMatch === null) {continue}
                    patchTime = (timeMatch[1] === undefined ? 0 : timeMatch[1] * 3600) +
                        (timeMatch[3] === undefined ? 0 : timeMatch[3] * 60) +
                        (timeMatch[4] === undefined ? 0 : timeMatch[4] * 1);
                    console.log("TIMEMATCH", patchTime, timeMatch);
                    if (newTime === undefined || patchTime < newTime) {
                        newTime = patchTime;
                    }
                }
                return (newTime < 20 ? 20 : newTime) * 1000;
            }
            console.log("New interval should be", calcInterval());
        });
    });
}, 0);

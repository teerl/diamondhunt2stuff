setTimeout(function()
           {
               let isDisplayed = function(selector)
               {
                   return document.querySelector(selector).style.display != "none";
               };
               for (potion of ["stardust", "superStardust"])
               {
                   if (isDisplayed("#notif-" + potion + "PotionTimer")) {return;}
               }
               let clickyChain = function(datas, timeDelay, afterFunc) {
                   setTimeout(function()
                              {
                                  if (datas.length > 0)
                                  {
                                      let data = datas.shift();
                                      if (typeof data == "string") {
                                          data = document.querySelector(data);
                                      }
                                      data.click();
                                      clickyChain(datas, timeDelay, afterFunc);
                                  }
                                  else if (afterFunc !== undefined)
                                  {
                                      afterFunc ();
                                  }
                              }, timeDelay);
               };
               clickyChain(["#tab-container-bar-brewing"], 100, function()
                           {
                               let potionsToDrink = [];
                               for (potion of ["stardust", "superStardust"])
                               {
                                   let potionSelector = "#item-box-" + potion + "Potion";
                                   if (!isDisplayed("#notif-" + potion + "PotionTimer") &&
                                       isDisplayed(potionSelector))
                                   {
                                       potionsToDrink.push(document.querySelector(potionSelector),
                                                           "#dialogue-confirm-yes");
                                   }
                               }
                               clickyChain(potionsToDrink, 500);
                           });
           }, 0);

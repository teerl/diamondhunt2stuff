setTimeout(function(){
    console.log("Fight", fightLocation);
    let teerlClickyChain = function(datas, timeDelay) {
        setTimeout(function() {
            let query = datas.shift();
            console.log("Clicking", query);
            document.querySelector(query).click();
            if (datas.length > 0) {
                teerlClickyChain(datas, timeDelay);
            }
        }, timeDelay);
    }
    teerlClickyChain(['#tab-container-bar-combat',
                      '#tab-sub-container-combat > span:nth-child(1)',
                      fightLocation,
                      '#dialogue-confirm-yes'],
                     750);
},0);

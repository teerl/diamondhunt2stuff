setTimeout(function() {
    if (document.querySelector('#notification-static-woodcutting').style.display == "none") {
        return;
    }
    console.log("Cut Trees");
    document.querySelector("#tab-container-bar-woodcutting").click();
    setTimeout(function() {
        let trees = [];
        for (const i of [1,2,3,4]) {
            var e = document.querySelector("#wc-div-tree-" + i);
            if (e.textContent == "(ready)") {
                trees.push(e);
            }
        }
        if (trees.length > 0) {
            let clickyChain = function(datas, timeDelay) {
                console.log("tree #" + trees.length);
                setTimeout(function() {
                    datas.shift().click();
                    if (datas.length > 0) {
                        clickyChain(datas, timeDelay);
                    }
                }, timeDelay);
            }
            clickyChain(trees, 900);
        }
    }, 100);
}, 0);

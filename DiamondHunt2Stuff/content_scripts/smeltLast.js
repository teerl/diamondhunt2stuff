setTimeout(function(){
    if (document.querySelector("#notif-smelting").style.display != "none") {
        return;
    }
    console.log("Smelt Last");
    let teerlClickyChain = function(datas, timeDelay) {
        setTimeout(function() {
            let query = datas.shift();
            console.log("Clicking", query);
            document.querySelector(query).click();
            if (datas.length > 0) {
                teerlClickyChain(datas, timeDelay);
            }
        }, timeDelay);
    }
    let furnaceSelector = function() {
        let furnaces = ["#item-box-boundStoneFurnace",
                        "#item-box-boundBronzeFurnace",
                        "#item-box-boundIronFurnace",
                        "#item-box-boundSilverFurnace",
                        "#item-box-boundGoldFurnace",
                        "#item-box-boundPromethiumFurnace",
                        "#item-box-boundRuniteFurnace",
                        "#item-box-boundDragoniteFurnace"];
        for (const q of furnaces) {
            const display = document.querySelector(q).style.display;
            if (display != "none") {
                return q;
            }
        }
        return undefined;
    }
    teerlClickyChain(['#tab-container-bar-crafting',
                      furnaceSelector(),
                      '#dialogue-furnace-mats-needed-area > input[type=button]'],
                     300);
},0);

function displayAutoStatus(action) {
    let intervalName = 'auto' + action + 'Interval';
    chrome.storage.local.get([intervalName], function(response) {
        document.getElementById(action + "AutoStatus").innerText =
            response[intervalName] === undefined ? "Off" : "On";
    });
}

function createAutoGroup(action) {
    let doButton = document.createElement('button');
    doButton.textContent = action.replace(/([A-Z])/g, ' $1').trimLeft();
    doButton.onclick = function(e) {
        chrome.runtime.sendMessage({id: action});
    };
    let autoButton = document.createElement('button');
    autoButton.textContent = 'Auto';
    autoButton.onclick = function(e) {
        chrome.runtime.sendMessage({id: action + 'Auto'}, function() {
            displayAutoStatus(action);
        });
    };
    let statusDiv = document.createElement('div');
    statusDiv.id = action + "AutoStatus";
    statusDiv.className = 'status';

    let parent = document.getElementById(action + 'Div');
    parent.className = 'wrapper';
    for (e of [doButton, autoButton, statusDiv]) {
        parent.appendChild(e)
    }
    displayAutoStatus(action);
};

for (action of "Planting SmeltLast DrinkStardustPotion CutTrees".split(' ')) {
    createAutoGroup(action);
}

document.getElementById ('CheckMarketButton').onclick = function(e) {
    chrome.runtime.sendMessage({id: "CheckMarket"});
};

document.getElementById ('FightButtonFields').onclick = function(e) {
    chrome.runtime.sendMessage({id: "Fight", where: "fields"});
};
document.getElementById ('FightButtonForest').onclick = function(e) {
    chrome.runtime.sendMessage({id: "Fight", where: "forest"});
};
document.getElementById ('FightButtonCaves').onclick = function(e) {
    chrome.runtime.sendMessage({id: "Fight", where: "caves"});
};
document.getElementById ('FightButtonVolcano').onclick = function(e) {
    chrome.runtime.sendMessage({id: "Fight", where: "volcano"});
};

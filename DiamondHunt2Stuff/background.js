chrome.runtime.onInstalled.addListener(function() {
    chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
        chrome.declarativeContent.onPageChanged.addRules([ {
            conditions: [new chrome.declarativeContent.PageStateMatcher({
                pageUrl: {hostEquals: "www.diamondhunt.co"}
            })],
            actions: [new chrome.declarativeContent.ShowPageAction()]
        }]);
    });
});

function executeScript(execObjs, sender, callback) {
    if (!Array.isArray(execObjs)) {execObjs = [execObjs];}
    else if (execObjs.length == 0) {return;}
    let execObj = execObjs.shift();
    console.log("Executing Script", execObj, sender, callback);
    let execRest = function() {executeScript(execObjs, sender, callback);};
    if (sender === undefined || sender.tab === undefined) {
        chrome.tabs.executeScript(execObj, execRest);
    } else {
        chrome.tabs.executeScript(sender.tab.id, execObj, execRest);
    }
}

function autoWhatever(actionName, sendResponse) {
    let intervalName = "auto" + actionName + "Interval";
    chrome.storage.local.get([intervalName], function(response) {
        console.log(response);
        if (response[intervalName] === undefined) {
            console.log("Turning On:", intervalName)
            let runMe = function(){
                console.log("Auto actionName procing");
                chrome.runtime.sendMessage({id: "actionName"});
            }.toString().replace(/actionName/g, actionName);
            chrome.tabs.executeScript({code: "setInterval("+runMe+",20000);"},function(results){
                let setMe = {};
                setMe[intervalName] = results[0];
                chrome.storage.local.set(setMe, function(){sendResponse();});
            });
        } else {
            chrome.storage.local.remove([intervalName], function() {
                console.log("Turning Off:", intervalName)
                chrome.tabs.executeScript({code: "clearInterval(" + response[intervalName] + ");"});
                sendResponse();
            });
        }
    });
}

chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
    let execScr = function(scriptName) {
        executeScript({file: 'content_scripts/' + scriptName + '.js'}, sender);
    };
    console.log("Request recieved:", request);
    if (request.id == "CheckMarket") {
        execScr('checkMarket');
    } else if (request.id == "SmeltLast") {
        execScr('smeltLast');
    } else if (request.id == "SmeltLastAuto") {
        autoWhatever("SmeltLast", sendResponse);
    } else if (request.id == "DrinkStardustPotion") {
        execScr('drinkStardustPotion');
    } else if (request.id == "DrinkStardustPotionAuto") {
        autoWhatever("DrinkStardustPotion", sendResponse);
    } else if (request.id == "Planting") {
        execScr('planting');
    } else if (request.id == "PlantingAuto") {
        autoWhatever("Planting", sendResponse);
    } else if (request.id == "CutTrees") {
        execScr('cutTrees');
    } else if (request.id == "CutTreesAuto") {
        autoWhatever("CutTrees", sendResponse);
    } else if (request.id == "Fight") {
        executeScript([{code: 'var fightLocation = "#' + request.where + '-tr";'},
                       {file: 'content_scripts/fight.js'}], sender);
    } else {
        alert("Unknown request", request);
    }
    return true;
});



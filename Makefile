.POSIX:
.SUFFIXES:
.PHONY: clean

SD = DiamondHunt2Stuff
JSFILES = $(SD)/popup.js $(SD)/background.js $(SD)/options.js $(SD)/content_scripts/checkMarket.js $(SD)/content_scripts/smeltLast.js $(SD)/content_scripts/planting.js $(SD)/content_scripts/fight.js $(SD)/content_scripts/cutTrees.js $(SD)/content_scripts/drinkStardustPotion.js
JSOFILES = $(JSFILES:.js=.jso)

DiamondHuntStuff.tgz: $(JSOFILES)
	./package
clean:
	rm -f $(JSOFILES)

.SUFFIXES: .js .jso
.js.jso:
	./closure-compiler $<
